package animalcorp

abstract class Animal() {
    fun walk() {
        print("walk")
    }
}
class Cow: Animal() {
    fun moo() {
        print("Moo")
    }
}

class Farm(val animals: List<Animal>)